﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trueplayer : MonoBehaviour
{
    public AnimationClip _walk, _jump;
    public Animation _Legs;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _Legs.clip = _walk;
        _Legs.Play();
    }

    // Update is called once per frame
    void Update()
    {
        _Legs.Play();
        rb.velocity = new Vector2(1f, rb.velocity.y);
    }
}
