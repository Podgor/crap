﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class headon : MonoBehaviour
{
    Rigidbody2D rb;
    public float angle;
    public bool torqueplus;
    public bool torqueminus;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

 
    void Update()
    {
        angle = transform.localEulerAngles.z;
        if (angle > 20 && angle < 180 || angle < -180 && angle > -360)
        {

            //transform.RotateAround(transform.parent.position, Vector3.forward * dir, sp * Time.deltaTime);
            transform.Rotate(90.0f, 0.0f, 0.0f, Space.Self);
            torqueminus = true;
            //rb.AddTorque(-5);
        }
        else
        { torqueminus = false; }
        if (angle < - 20 && angle > - 180 || angle > 180 && angle < 360)
        {
            //rb.AddTorque(5);
            torqueplus = true;
        }
        else
        { torqueplus = false; }
    }
}
