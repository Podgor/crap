﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloud : MonoBehaviour
{
    Vector3 target_pos;
    public GameObject target;
    public 
    // Start is called before the first frame update
    void Start()
    {
        //target = GetComponentInChildren<Transform>().position;
        //target_pos = transform.root.GetComponent<Transform>().position;
        target_pos = target.transform.position;
        target.GetComponent<SpriteRenderer>().enabled = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target_pos, 0.4f);
    }
}
