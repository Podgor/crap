﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class botscript : MonoBehaviour
{

    public GUIStyle CustomButton;
    Rigidbody2D rb;
    Rigidbody2D rrb;
    public Joystick joystick;
    Animator anim;
    float speed = 3f;
    public GameObject magic_pref;
    float run = 0;
    bool whenlook = true;
    int kakafka = 0;
    float local_rot;
    GameObject bike;
    GameObject hand;
    GameObject head;
    GameObject stick;
    GameObject flying;
    [System.NonSerialized]
    public GameObject onstickmagic;
    public GameObject flyingmagic;
    public int hellth = 5;
    JointAngleLimits2D angles1 = new JointAngleLimits2D();
    JointAngleLimits2D angles2 = new JointAngleLimits2D();
    JointAngleLimits2D angles3 = new JointAngleLimits2D();
    JointAngleLimits2D angles4 = new JointAngleLimits2D();
    JointAngleLimits2D angles5 = new JointAngleLimits2D();
    public Action ac;


    public Vector3 targetPoint;
    [System.NonSerialized]
    public List<GameObject> GOinTrigger = new List<GameObject>();
   

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<GmObj>())
        {

            //print(other);
            if (other.GetComponent<GmObj>().selectable)
            {
                if (other.GetComponent<GmObj>().is_firstly_selected == false)
                {
                    other.GetComponent<particles>().OnEnter();
                    other.GetComponent<GmObj>().is_firstly_selected = true;
                    onstickmagic.GetComponent<ParticleSystem>().startSize = 0.2f;
                    hand.GetComponent<HingeJoint2D>().limits = angles2;
                    hand.GetComponent<Rigidbody2D>().AddTorque(30);
                 
                }
                else
                {
                    other.GetComponent<GmObj>().is_selected = true;
                    //other.GetComponent<GmObj>().ps.Play();
                    GOinTrigger.Add(other.gameObject);

                    onstickmagic.GetComponent<ParticleSystem>().startSize = 0.4f;
                    flyingmagic.GetComponent<ParticleSystem>().Play();
                    hand.GetComponent<HingeJoint2D>().limits = angles3;
                    hand.GetComponent<Rigidbody2D>().AddTorque(40);


                    Vector3 targetPoint = other.transform.position;

                }

            }
        }
        else if (other.gameObject.GetComponentInParent<ropee>())
        {
            if (other.gameObject.tag == "ropend")
            {
                if (other.gameObject.GetComponentInParent<ropee>().is_firstly_selected)
                {
                    GOinTrigger.Add(other.gameObject);
                    //print(other);
                    other.gameObject.GetComponentInParent<ropee>().is_selected = true;
                } 
                else
                {
                    other.gameObject.GetComponentInParent<ropee>().is_firstly_selected = true;
                }
            }
        }

        else if (other.tag == "monster" || other.tag == "bomb")
        {
            if (other.GetComponent<bomb>().selected)
            {
                GOinTrigger.Add(other.gameObject);
                if (GameObject.Find("Shield").GetComponent<SpriteRenderer>().enabled)
                {
                    GameObject.Find("Shield").GetComponent<Shield>().start_cor();
                    var tang = Math.Abs(other.transform.position.y - transform.position.y) / Math.Abs(other.transform.position.x - transform.position.x);
                    float sin = (float)Math.Sin(Math.Atan(tang));
                    other.GetComponent<Rigidbody2D>().AddForce(other.GetComponent<Rigidbody2D>().velocity * new Vector2(1.5f, -1.5f), ForceMode2D.Impulse);

                }
                

            }
            else
            {
                
                other.GetComponent<bomb>().selected = true;
            }

            

            
        }

    }

    void OnTriggerExit2D(Collider2D other)

    {
        if (other.GetComponent<GmObj>())
        {
            if (other.GetComponent<GmObj>().is_selected)
            {
                GOinTrigger.Remove(other.gameObject);
                other.GetComponent<GmObj>().is_selected = false;
                
                if (GOinTrigger.Count == 0)
                {
                    hand.GetComponent<HingeJoint2D>().limits = angles2;
                    hand.GetComponent<Rigidbody2D>().AddTorque(-12);
                    onstickmagic.GetComponent<ParticleSystem>().startSize = 0.2f;
                    flyingmagic.GetComponent<ParticleSystem>().Stop();
                }

            }
            else
            {
                other.GetComponent<GmObj>().is_firstly_selected = false;
                other.GetComponent<particles>().OnExit();
                if (GOinTrigger.Count == 0)
                {
                    hand.GetComponent<HingeJoint2D>().limits = angles1;
                    hand.GetComponent<Rigidbody2D>().AddTorque(-12);
                    onstickmagic.GetComponent<ParticleSystem>().startSize = 0.1f;
                }


            }


        }
        else if (other.gameObject.GetComponentInParent<ropee>())
        {
            if (other.gameObject.tag == "ropend")
            {
                if (other.gameObject.GetComponentInParent<ropee>().is_selected)
                {
                    GOinTrigger.Remove(other.gameObject);
                    other.gameObject.GetComponentInParent<ropee>().is_selected = false;
                }
                else
                {
                    other.gameObject.GetComponentInParent<ropee>().is_firstly_selected = false;

                }
            }
        }
        else if (other.tag == "monster" || other.tag == "bomb")
        {
            if (other.GetComponent<bomb>().selected)
            {

                other.GetComponent<bomb>().selected = false;
            }
            else
            {
                GOinTrigger.Remove(other.gameObject);
            }


            
        }

    }



    void Start()
    {
        ac = reduce;

        //Time.timeScale = 0.5f;
         

        rb = GameObject.Find("bike").GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        hand = GameObject.Find("hand left");
        bike = GameObject.Find("bike");
        stick = GameObject.Find("stick");
        head = GameObject.Find("head");
        angles1.min = -10;
        angles1.max = 10;
        angles2.min = -70;
        angles2.max = -65;
        angles3.min = -90;
        angles3.max = -85;
        angles4.min = -20;
        angles4.max = 30;
    
        
        float speed = 5;
        onstickmagic = GameObject.Find("onstickmagic");
        flyingmagic = GameObject.Find("flyingmagic");
        flyingmagic.GetComponent<ParticleSystem>().Stop();
        foreach (GameObject roma in GOinTrigger)
        {
           // print(roma);
        }
        //print("finish");
        
    }


    public void breakk()
    {
        kakafka = 0;
    }
    public void gas()
    {
        speed = 3f;
    }

    public void reduce()
    {


        stick.GetComponent<HingeJoint2D>().limits = angles4;
        stick.GetComponent<Rigidbody2D>().AddTorque(-100);
        foreach (GameObject G in GOinTrigger)
        {
            if (G.GetComponent<GmObj>())
            {
                
                if (G.GetComponent<GmObj>().reducible)
                {
                    targetPoint = G.transform.position;
                    flying = Instantiate(magic_pref, new Vector3(onstickmagic.transform.position.x, onstickmagic.transform.position.y, onstickmagic.transform.position.z), Quaternion.identity);
                    flying.GetComponent<flyingmagic>().aim = G;
                    flying.GetComponent<flyingmagic>().action = G.GetComponent<GmObj>().reduce;

                }
            }
        }
    }

    public void ungravity()
    {
       // print("reversed");
        //print(GOinTrigger);
        foreach (GameObject G in GOinTrigger)
        {
            rrb = G.GetComponent<Rigidbody2D>();

            if (rrb != null && G.GetComponent<GmObj>().soarable)
            {
                targetPoint = G.transform.position;
                flying = Instantiate(magic_pref, new Vector3(onstickmagic.transform.position.x, onstickmagic.transform.position.y, onstickmagic.transform.position.z), Quaternion.identity);
                flying.GetComponent<flyingmagic>().aim = G;
                flying.GetComponent<flyingmagic>().action = G.GetComponent<GmObj>().ungravity;

            }
        }
    }

    public void cut()
    {
        foreach (GameObject rope in GOinTrigger)
        {
            if (rope.gameObject.GetComponentInParent<ropee>())
            {
                rope.gameObject.GetComponentInParent<ropee>().cut();
            }
        }
    }


    public void refucking()
    {
        GOinTrigger.Clear();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }





    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            Worms();

        }
        if (joystick.Vertical > 0.4 && kakafka == 0)
        {
            kakafka = 1;
            Worms();
            Invoke("breakk", 2f);

        }


        //Time.fixedDeltaTime = Time.deltaTime * 0.5f;
        //if (joystick.Horizontal > 0.1)
        //{
        //    run = 3f;
        //    whenlook = true;

        //    Flip();
        //}

        //else if (joystick.Horizontal < -0.1)
        //{
        //    run = -3f;
        //    whenlook = false;

        //    Flip();
        //}
        //else
        //{
        //    run = 0;

        //}
        rb.velocity = new Vector2(3f * joystick.Horizontal, rb.velocity.y);

        local_rot = bike.transform.eulerAngles.z;
        if (180 > local_rot && local_rot > 20)
        {
            rb.AddTorque(-15);
        }
        if (340 > local_rot && local_rot > 180)
        {
            rb.AddTorque(15);
        }

    }

    void Flip()
    {
        if (whenlook)
            transform.localRotation = Quaternion.Euler(0, 0, 0);

        if (!whenlook)
            transform.localRotation = Quaternion.Euler(0, 180, 0);
    }

    public void Worms()
    {
        //head.GetComponent<FixedJoint2D>().enabled = true;
       
        rb.AddForce(transform.up * 18f, ForceMode2D.Impulse);
       // head.GetComponent<FixedJoint2D>().enabled = false;
    }


}
