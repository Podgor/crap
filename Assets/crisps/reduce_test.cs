﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class reduce_test : MonoBehaviour
{
    public Vector3 inert_scale_size;
    Vector3 current_scale_size;
    public bool is_reduced = false;
    public bool is_firstly_selected = false;
    public UnityEvent Reduced;
    public UnityEvent Reducible;
    bool a = false;

    void Start()
    {
        inert_scale_size = transform.localScale;

    }

    

    void Update()
    {
        
        if (a)
        {
            transform.localScale = Vector3.Lerp(current_scale_size, inert_scale_size, Time.time / 10);
        }
            
            


    }


    void OnMouseDown()
    {

        is_reduced = GetComponent<GmObj>().is_reduced;
        is_firstly_selected = GetComponent<GmObj>().is_firstly_selected;

        if (is_reduced)
        {
            Reduced.Invoke();
        
        }
        else
        {
            Reducible.Invoke();
          
        }



        float coef = GetComponent<GmObj>().scale_coef;
        if (this.GetComponent<GmObj>().is_reduced == false)
        {
            current_scale_size = transform.localScale;
            //transform.localScale = new Vector3(inert_scale_size.x * coef, inert_scale_size.y * coef, 1);
            this.GetComponent<GmObj>().is_reduced = true;
            a = true;
           
        }
        else
        {
            transform.localScale = inert_scale_size;
            this.GetComponent<GmObj>().is_reduced = false;
          
        }



     




    }
}
