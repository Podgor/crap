﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GmObj : MonoBehaviour
{

    public bool selectable = false;
    public bool soarable = false;
    public bool is_ungravited = false;
    public bool reducible = false;
    public bool is_reduced = false;
    public bool is_selected = false;
    public bool destroyable = false;
    public bool forceable = false;
    float inert_gravity;
    public float scale_coef = 0.4f;
    int i;
   
   
    public bool is_firstly_selected = false;
    public Vector3 inert_scale_size;
    public Vector3 inert_position;
    Rigidbody2D rrb;




    void Start()
    {

        if (reducible)
        {
           
            inert_scale_size = transform.localScale;
            

            if (is_reduced)
            {
                transform.localScale = new Vector3(inert_scale_size.x * scale_coef, inert_scale_size.y * scale_coef, 1);
             
            }

        }

        if (soarable)
        {
            Rigidbody2D rb = GetComponent<Rigidbody2D>();
            inert_gravity = rb.gravityScale;
            if (is_ungravited)
            {
                rb.gravityScale *= -1;
            }
        }

    }

  
    public void reduce()
    {
        
        inert_position = transform.position;
        if (is_reduced)
        {

            StartCoroutine(DelayEnlarge());
        }
        else
        {

            StartCoroutine(DelayReduce());

            
        }
        
    }

    public void ungravity()
    {
        rrb = GetComponent<Rigidbody2D>();

        if (rrb != null && soarable)
        {
            
           rrb.gravityScale *= -1;
            if (is_ungravited)
            {
                
                GetComponent<particles>().OnGravity();
                is_ungravited = false;
            }
            else
            {
                
                GetComponent<particles>().OffGravity();
                is_ungravited = true;
            }

        }
    }
    public IEnumerator DelayReduce()
    {

            
            for (i = 10; i < 10 / scale_coef; i++)
            {
                
                transform.localScale = new Vector3(inert_scale_size.x / i * 10, inert_scale_size.y / i * 10, 1);
                
                transform.position = new Vector3(inert_position.x, inert_position.y - 3.5f*inert_scale_size.y + 3.5f*inert_scale_size.y/i*10, inert_position.z);
                
                yield return new WaitForSeconds(0);
          
               
            }
            GetComponent<particles>().OnReduce();
            is_reduced = true;
       
    }

    public IEnumerator DelayEnlarge()
    {
        GameObject bike = GameObject.Find("bike");
        float deltaposx = Mathf.Abs (bike.transform.position.x - transform.position.x);
        float deltaposy = Mathf.Abs(bike.transform.position.y - transform.position.y);
        if (deltaposx < inert_scale_size.x * scale_coef * 4 && deltaposy < inert_scale_size.y * scale_coef * 3.5 + bike.transform.localScale.y * 3.5 && bike.transform.position.y > transform.position.y)
            
        {
            GameObject.Find("bike").GetComponent<botscript>().Worms();
        }
        
        for (i = 10; i < 10 / scale_coef; i++)
        {

            
            transform.localScale = new Vector3(inert_scale_size.x * i / 10 * scale_coef, inert_scale_size.y * i * scale_coef / 10, 1);            
            transform.position = new Vector3(inert_position.x, inert_position.y - 3.5f * inert_scale_size.y*scale_coef + 3.5f * inert_scale_size.y * i / 10*scale_coef, inert_position.z);
            yield return new WaitForSeconds(0);

        }
        
        is_reduced = false;
        GetComponent<particles>().OffReduce();  

    }

}
