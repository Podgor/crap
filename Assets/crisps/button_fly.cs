﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class button_fly : MonoBehaviour
{
    public Button hat;
    public Button button;
    public GameObject pocket;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, hat.transform.position, 2.2f);
        Vector3 pos = transform.position;
        Vector3 pos1 = hat.transform.position;
        if (pos1.x + 5 > pos.x && pos.x>pos1.x - 5 && pos1.y + 5 > pos.y && pos.y > pos1.y - 5)
        {
            Destroy(this.gameObject);
            hat.GetComponent<hat_scale>().blump();
            pocket.GetComponent<pocket>().add_magic(button);
        }
    }
}
