﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger2 : MonoBehaviour
{
    public bool activated = false;
    public GameObject doll;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && activated == false)
        {
            activated = true;
            Invoke("start_mad", 1);

        }
    }
    // Update is called once per frame
    void start_mad()
    {
        doll.GetComponent<maddoll>().move = true;
        doll.GetComponent<DragonBones.UnityArmatureComponent>().animation.Play("move");
        doll.GetComponent<Rigidbody2D>().simulated = true;
    }
    void Update()
    {
        
    }
}
