﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flyingmagic : MonoBehaviour
{
    public GameObject aim;
    public System.Action action;



    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, aim.transform.position, 0.05f);
    }
    void OnTriggerEnter2D(Collider2D G)
    {
         
        if (G.gameObject == aim)
        {

            aim.GetComponent<particles>().OnMagic();
            StartCoroutine(Delay());
            

        }
    }

    IEnumerator Delay()
    {
        while (true)
        {
            
            yield return new WaitForSeconds(0.3f);
            Destroy(this.gameObject);
            action();
        }
    }
}
