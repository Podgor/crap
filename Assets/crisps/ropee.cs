﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ropee : MonoBehaviour
{

    public GameObject mag;
    HingeJoint2D[] hingeJoints;
    public bool collidered = false;
    BoxCollider2D[] boxColliders;
    public bool is_firstly_selected = false;
    public bool is_selected = false;

    // Start is called before the first frame update
    void Start()
    {
        if (collidered)
        {
            boxColliders = GetComponentsInChildren<BoxCollider2D>();
            foreach (BoxCollider2D col in boxColliders)
            {
                col.enabled = true;
                col.offset = new Vector2(-0.03f, -0.91f);
                col.size = new Vector2(2.89f, 0.69f);
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void cut()
    {
        
        hingeJoints = GetComponentsInChildren<HingeJoint2D>();
        hingeJoints[4].enabled = false;
    }
}
