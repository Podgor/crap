﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hat_scale : MonoBehaviour
{
    public void blump()
    {
        StartCoroutine(blum());
    }
    
    IEnumerator blum()
    {
        for (float i = 1; i<1.4; i += 0.05f)
        {
            transform.localScale = new Vector3(i, i, 1);
            yield return new WaitForSeconds(0.02f);
        }
        for (float i = 1.4f; i > 1; i -= 0.05f)
        {
            transform.localScale = new Vector3(i, i, 1);
            yield return new WaitForSeconds(0.02f);
        }
    }
}
