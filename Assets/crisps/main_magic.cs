﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class main_magic : MonoBehaviour
{
    public System.Action action;

    public void use_main()
    {
        action();
    }
}
