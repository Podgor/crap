﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class materialing : MonoBehaviour
{
    //public static List<GameObject> GO = new List<GameObject>();
    GameObject[] GO;
    public Material l_material;
    // Start is called before the first frame update
    void Start()
    {
        GO = GameObject.FindObjectsOfType<GameObject>();
        foreach (GameObject w in GO)
        {
            if (w.GetComponent<SpriteRenderer>())
            {
                w.GetComponent<SpriteRenderer>().material = l_material;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
