﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UIElements;
using UnityEngine.UI;

public class new_button : MonoBehaviour
{
    public GameObject empty;
    public Button hat;
    public Canvas can;
    public Image im;
    public Button button;
    GameObject button_sprite;
    bool collected = false;
    // Start is called before the first frame update
    void Start()
    {
        button_sprite = Instantiate(empty, transform.position, Quaternion.identity);
        button_sprite.AddComponent<SpriteRenderer>();
        button_sprite.GetComponent<SpriteRenderer>().sprite = button.image.sprite;
        button_sprite.AddComponent<Rigidbody2D>();
        button_sprite.GetComponent<Rigidbody2D>().gravityScale = 0;
        button_sprite.AddComponent<trapping>();
        button_sprite.AddComponent<self_button>();
        button_sprite.GetComponent<self_button>().im = im;
        button_sprite.GetComponent<self_button>().can = can;
        button_sprite.GetComponent<self_button>().hat = hat;
        button_sprite.GetComponent<trapping>().speed = 0.4f;
        button_sprite.GetComponent<trapping>().delta_y = 3f;
        button_sprite.AddComponent<CircleCollider2D>();
        button_sprite.GetComponent<CircleCollider2D>().isTrigger = true;
        button_sprite.transform.SetParent(transform);
    }
        // Update is called once per frame
    void Update()
    {
        if (collected)
        {
            //button_sprite.transform.position = Vector2.MoveTowards(button_sprite.transform.position, )
        }
    }


}
