﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bomb_away : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("away", 3);
    }

    // Update is called once per frame
    void away()
    {
        GameObject.Find("bomb2").transform.SetParent(null);
    }
}
