﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class nominal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("reload", 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void reload()
    {
        if (GetComponentInParent<card>().color == op.red)
        {
            GetComponent<Text>().color = new Color32(167, 9, 9, 255);
        }

        else if (GetComponentInParent<card>().color == op.black)
        {
            GetComponent<Text>().color = new Color32(0, 0, 0, 255);
        }
    }

    public void add_num(int num)
    {
        GetComponent<Text>().text = (int.Parse(GetComponent<Text>().text) + num).ToString();
    }
}
