﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class maddoll : monster
{
    Rigidbody2D rb;
    public float speed = 10f;
    
    public bool move = true;
    int dir = -1;
    public override bool angry
    {
        get
        {
            return angry;
        }

        set
        {
            if (value == false)
            {
                GetComponent<DragonBones.UnityArmatureComponent>().animation.Play("stay");
                dir = 0;
            }

            else
            {
                GetComponent<DragonBones.UnityArmatureComponent>().animation.Play("move");
                dir = 1;
            }

            angry = value;
        }
    }
    //public bool right = true;

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            bike.GetComponent<botscript>().refucking();
        }
        else if (collision.gameObject.tag == "spike")
        {
            collapse();
        }
    }


    
    public override void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        speed = 4;
        move = false;
        base.Start();
    }

    void Update()
    {
        if (angry)
        {
            dir = (transform.position.x < bike.transform.position.x) ? 1 : -1;
        }


        if (move)
        { 
            transform.Translate(new Vector3(dir, 0, 0)*Time.deltaTime);
        }
    }
}