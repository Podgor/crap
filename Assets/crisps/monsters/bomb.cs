﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bomb : MonoBehaviour
{
    Rigidbody2D rb;
    public bool selected = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.AddTorque(-20);//Random.Range(-10, 10));
        Invoke("explode", 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void explode()
    {
        Destroy(this.gameObject);
    }
}
