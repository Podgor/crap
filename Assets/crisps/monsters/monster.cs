﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monster : MonoBehaviour
{

    public GameObject bike;
    public virtual bool angry { get; set; }
    // Start is called before the first frame update
    public virtual void Start()
    {
        bike = GameObject.Find("bike");
        tag = "monster";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void collapse()
    {

        //print(GetComponent<DragonBones.UnityArmatureComponent>().animationName);
        GetComponent<DragonBones.UnityArmatureComponent>().animation.Stop();
        foreach (Transform child in GetComponentsInChildren<Transform>())
        {
            child.gameObject.AddComponent<Rigidbody2D>();

        }

    }
}
