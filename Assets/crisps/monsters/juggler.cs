﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class juggler : monster
{
    public GameObject bomb;
    // Start is called before the first frame update
    public override void Start()
    {
        Invoke("throw_bomb", 3);
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void throw_bomb()
    {
        GameObject the_bomb = Instantiate(bomb, transform.position + new Vector3(1.72f, 0.8f, 0)*transform.localScale.x, Quaternion.identity);
        the_bomb.GetComponent<Rigidbody2D>().AddForce(new Vector2(-7, 12), ForceMode2D.Impulse);
        the_bomb.tag = "bomb";
    }
}
