﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pocket : MonoBehaviour
{
    public Vector3 start_pos;
    public Vector3 target;
    bool showed = false;
    bool move = false;
    // Start is called before the first frame update
    void Start()
    {
        start_pos = GetComponent<RectTransform>().anchoredPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (move)
        {
            GetComponent<RectTransform>().anchoredPosition = Vector3.MoveTowards(GetComponent<RectTransform>().anchoredPosition, target, 10f);
            if (System.Math.Abs(GetComponent<RectTransform>().anchoredPosition.x-target.x) < 1)
            {
                move = false;
            }
        }
    }
    public void show_pocket()
    {
        if (!showed)
        {
            Vector3 target_pos = new Vector3(start_pos.x - GetComponentsInChildren<Button>().Length*293 - 300, start_pos.y, start_pos.z);
            print(GetComponentsInChildren<Button>().Length);
            target = target_pos;
            move = true;
            showed = true;
        }
        else
        {
            target = start_pos;
            move = true;
            showed = false;
        }
    }

    public void add_magic(Button but)
    {
        Button b = Instantiate(but, Vector3.zero, Quaternion.identity);
        b.transform.SetParent(transform);
        b.GetComponent<RectTransform>().anchoredPosition = new Vector3(-GetComponent<RectTransform>().rect.width + 267*(GetComponentsInChildren<Button>().Length-1) + 145, 135.6f, 0);
        b.transform.localScale = Vector3.one;
    }
}
