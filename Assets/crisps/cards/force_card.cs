﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class force_card : card
{
    public float force = 10.4f;
    // Start is called before the first frame update
    public override void Start()
    {

        reload_time = 3;
        suit = suits.spade;
        base.Start();
    }

    public override void use_magic()
    {
        foreach (GameObject G in bocs.GOinTrigger)
        {

            if ((G.GetComponent<GmObj>() && G.GetComponent<GmObj>().forceable) || G.tag == "monster")
            {
                var tang = Math.Abs(G.transform.position.y - bot.transform.position.y)/ Math.Abs(G.transform.position.x - bot.transform.position.x);
                float sin = (float)Math.Sin(Math.Atan(tang));
                G.GetComponent<Rigidbody2D>().AddForce(new Vector2(sin*force/tang, sin*force + 2),ForceMode2D.Impulse);
                //G.GetComponent<Rigidbody2D>().AddForce(new Vector2(20, 20), ForceMode2D.Impulse);
                //Debug.Log("lalalararara");
            }
        }
    }
}
