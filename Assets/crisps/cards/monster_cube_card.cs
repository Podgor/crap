﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monster_cube_card : card
{

    public override void Start()
    {

        reload_time = 14;
       
        suit = suits.spade;
        base.Start();
    }

    public override void use_magic()
    {
        GameObject cube = Resources.Load<GameObject>("Prefabs/box");
        foreach(GameObject G in bocs.GOinTrigger)
        {
            if (G.tag == "monster")
            {
                Destroy(G);
                GameObject g = Instantiate(cube, G.transform.position, Quaternion.identity);
            }
        }

    }
}
