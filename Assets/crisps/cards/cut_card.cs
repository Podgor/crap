﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cut_card : card
{
    public override void Start()
    {
        suit = suits.club;
        reload_time = 5;
        base.Start();
    }

    public override void use_magic()
    {
        foreach (GameObject rope in bocs.GOinTrigger)
        {
            if (rope.gameObject.GetComponentInParent<ropee>())
            {
                rope.gameObject.GetComponentInParent<ropee>().cut();

            }
        }
    }
}
