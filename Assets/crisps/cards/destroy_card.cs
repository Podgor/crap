﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroy_card : card
{
    public override void Start()
    {
        reload_time = 6;
        suit = suits.club;
        base.Start();
    }

    void destroy(GameObject G)
    {
        G.SetActive(false);
    }

    public override void use_magic()
    {
        foreach (GameObject G in bocs.GOinTrigger)
        {
            if (G.GetComponent<GmObj>())
            {
                if (G.GetComponent<GmObj>().destroyable)
                {
                    send_flying(() => destroy(G), G);
                }
            }
        }
    }
}
