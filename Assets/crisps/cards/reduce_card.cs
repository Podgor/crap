﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reduce_card : card
{
    public override void Start()
    {
        suit = suits.club;
        base.Start();
    }

    void reduce(GameObject G)
    {
        G.GetComponent<GmObj>().inert_position = G.transform.position;
        if (G.GetComponent<GmObj>().is_reduced)
        {

            StartCoroutine(G.GetComponent<GmObj>().DelayEnlarge());
        }
        else
        {

            StartCoroutine(G.GetComponent<GmObj>().DelayReduce());


        }
    }

    public override void use_magic()
    {
        foreach (GameObject G in bocs.GOinTrigger)
        {
            if (G.GetComponent<GmObj>())
            {

                if (G.GetComponent<GmObj>().reducible)
                {
                    send_flying(() => reduce(G), G);

                }
            }
        }
    }
}
