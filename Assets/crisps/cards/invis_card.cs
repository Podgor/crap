﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class invis_card : card
{
    float duration = 4f;

    public override void Start()
    {
        suit = suits.heart;
        reload_time = 5;
         
        base.Start();
    }

    void off()
    {
        foreach (GameObject monst in GameObject.FindGameObjectsWithTag("monster"))
        {
            monst.GetComponent<monster>().angry = true;
        }
    }

    public override void use_magic()
    {
        foreach (GameObject monst in GameObject.FindGameObjectsWithTag("monster"))
        {
            monst.GetComponent<monster>().angry = false;
        }
        Invoke("off", duration);
    }

}
