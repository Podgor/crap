﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shield_card : card
{
    public float off_time;
    GameObject shield;

    public override void Start()
    {
        shield = GameObject.Find("Shield");
        reload_time = 12;
        off_time = 6;
        suit = suits.heart;
        base.Start();
    }

    void off()
    {
        shield.GetComponent<SpriteRenderer>().enabled = false;
    }

   

    public override void use_magic()
    {
        shield.GetComponent<SpriteRenderer>().enabled = true;
        Invoke("off", off_time);
    }
}
