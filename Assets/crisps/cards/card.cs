﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public enum op
{
    black = 0,
    red = 1
}

public enum suits 
{
    heart = 0,
    diamond = 1,
    club = 2,
    spade = 3

}

public class card : MonoBehaviour
{
    [System.NonSerialized]
    public Button main;
    public bool ready = true;
    //public UnityEvent tryy = new UnityEvent();
    public System.Action the_action;
    public bool in_pocket = true;
    public bool chose = false;
    //[System.NonSerialized]
    //public string color_ = "black";
    //[System.NonSerialized]
    [System.NonSerialized] 
    public op color;


    [System.NonSerialized]
    public suits suit;
    [System.NonSerialized]
    public float reload_time;

    [System.NonSerialized]
    public GameObject bot;
    public botscript bocs;
    public UnityAction unityAction;

    public virtual void Start()
    {
        unityAction += change;
        GetComponent<Button>().onClick.AddListener(unityAction);
        if (suit == suits.diamond || suit == suits.heart)
        {
            color = op.red;
        }
        else
        {
            color = op.black;
        }
        
        
        Debug.Log("card start");
        bot = GameObject.Find("bike");
        bocs = bot.GetComponent<botscript>();
        main = GameObject.Find("main_magic").GetComponent<Button>();
        
        //GameObject txt = new GameObject("dfd", typeof(Text));
        //txt.transform.SetParent(transform);
        
        //txt.GetComponent<Text>().text = "4";
        //txt.GetComponent<Text>().fontSize = 261;
        //txt.AddComponent<RectTransform>();
        //txt.layer = 5;
        //txt.transform.position = new Vector3(-9.5f, 108, 0);
        //txt.transform.localScale = new Vector3(0.2f, 0.2f, 1);
    }

    public void change()
    {
        if (in_pocket && !chose)
        {
            //main.image.sprite = this.gameObject.GetComponent<Button>().image.sprite;
            //main.GetComponent<main_magic>().action = tryy.Invoke;
            //GetComponentInParent<pocket>().show_pocket();
            if (main.gameObject.GetComponentsInChildren<Transform>().Length >= 2)
            {
                Destroy(main.gameObject.GetComponentsInChildren<Transform>()[1].gameObject);
            }
            GameObject using_button = Instantiate(this.gameObject, Vector3.zero, Quaternion.identity);
            using_button.transform.SetParent(main.gameObject.transform);
            using_button.transform.position = main.gameObject.transform.position;
            using_button.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
            main.GetComponent<Image>().enabled = false;
            using_button.GetComponent<card>().chose = true;
            print(using_button.GetComponent<card>());
            //using_button.GetComponent<> chose = true;
        }

        else if (in_pocket && chose && ready)
        {
            //tryy.Invoke();
            use_magic();
            ready = false;
            StartCoroutine(reloading());
            
        }

        
    }

    public IEnumerator reloading()
    {
        var temp_card = Instantiate(GetComponent<Image>(), transform);
        var temp_nominal = Instantiate(GetComponentInChildren<nominal>(), GetComponentInChildren<nominal>().transform);
        temp_card.transform.SetParent(transform.parent);
        temp_card.transform.position = transform.position;
        temp_card.transform.localScale = transform.localScale;
        temp_card.transform.SetAsFirstSibling();
        temp_nominal.transform.SetParent(temp_card.transform);
        temp_card.color = new Color32(100, 100, 100, 255);
        temp_nominal.GetComponent<Text>().color = new Color32(100, 100, 100, 255);
        GetComponent<Image>().type = Image.Type.Filled;
        GetComponent<Image>().fillAmount = 0;

        for (float i = 0; i<=1; i += 0.01f)
        {
            GetComponent<Image>().fillAmount = i;
            yield return new WaitForSecondsRealtime(reload_time/100);
        }

        Destroy(temp_card.gameObject);
        GetComponent<Image>().type = Image.Type.Simple;

        ready = true;
    }

    public void level_up()
    {
        GetComponentInChildren<nominal>().add_num(1);
    }

    public virtual void use_magic()
    {
        
    }

    public void send_flying(System.Action action, GameObject G)
    {
        
        var flying = Instantiate(Resources.Load<GameObject>("Prefabs/magic_pref"), bot.GetComponent<botscript>().onstickmagic.transform.position, Quaternion.identity);
        flying.GetComponent<flyingmagic>().aim = G;
        flying.GetComponent<flyingmagic>().action = action;

    }
}
