﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gravity_card : card
{

    

    public override void Start()
    {
        suit = suits.diamond;
        reload_time = 3;
        base.Start();
    }

    void ungravity(GameObject G)
    {
        Rigidbody2D rrb = G.GetComponent<Rigidbody2D>();

        GmObj obj = G.GetComponent<GmObj>();

        if (rrb != null && obj.soarable)
        {

            rrb.gravityScale *= -1;
            if (obj.is_ungravited)
            {
            
                G.GetComponent<particles>().OnGravity();
                obj.is_ungravited = false;
            }
            else
            {

                G.GetComponent<particles>().OffGravity();
                obj.is_ungravited = true;
            }

        }
    }

    public override void use_magic()
    {
        Debug.Log("wtf man");
        foreach (GameObject game in bocs.GOinTrigger)
        {
            if (game.GetComponent<GmObj>())
            {
                if (game.GetComponent<GmObj>().soarable)
                {
                    send_flying(() => ungravity(game), game);
                }

            }
        }

        //base.use_magic();
    }

}
