﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trapping : MonoBehaviour
{
    Rigidbody2D rb;
    public bool flipped = false;
    float y;
    public Sprite back;
    Sprite front;
    bool coroutine = false;
    public float speed = 1.2f;
    public float delta_y = 3;
    // Start is called before the first frame update
    void Start()
    {
        front = GetComponent<SpriteRenderer>().sprite;
        rb = GetComponent<Rigidbody2D>();
        y = transform.position.y;
        transform.position += new Vector3(0, delta_y/2, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.y<0.1f && transform.position.y > y && gameObject.tag == "card_loot" && !coroutine)
        {
            print(2345645);
            coroutine = true;
            StartCoroutine(flip_around());
        }

        if(transform.localEulerAngles.y > 90 && transform.localEulerAngles.y < 270 && !flipped)
        {
            GetComponent<SpriteRenderer>().sprite = back;
            flipped = true;

        }
        else if (flipped && (transform.localEulerAngles.y < 90 || transform.localEulerAngles.y > 270))
        {
            GetComponent<SpriteRenderer>().sprite = front;
            flipped = false;
        }
        //print(transform.localEulerAngles.y);
        rb.AddRelativeForce(new Vector2(0, (y - transform.position.y)*speed), ForceMode2D.Force);
    }

    IEnumerator flip_around()
    {
        for (int i=0; i<360; i+=2)
        {
            transform.localEulerAngles += new Vector3(0, 2, 0);
            yield return new WaitForSeconds(0);
        }
        coroutine = false;
    }
}
