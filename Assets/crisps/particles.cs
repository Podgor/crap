﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particles : MonoBehaviour
{
    public bool is_reduced = false;
    public bool is_ungravited = false;
    public ParticleSystem[] systems;




    void Start()
    {
        
        systems = GetComponentsInChildren<ParticleSystem>();
        systems[0].startSize = 8 * transform.localScale.x;
        systems[1].startSize = 8 * transform.localScale.x;
        foreach (ParticleSystem ps in systems)
        {
            ps.Stop();
            
        }

    }

    public void OnEnter ()
    {

        
        if (is_reduced)
        {
            systems[0].startSize = 8 * transform.localScale.x;
            systems[0].Play();
        }
        else
        {
            systems[1].Play();
        }

        if (is_ungravited)
        {
            systems[3].Play();
        }
        else
        {
            systems[2].Play();
        }


    }
    public void OnExit()
    {
        foreach (ParticleSystem ps in systems)
        {
            ps.Stop();

        }
    }
    public void OnReduce()
    {
        
        systems[0].startSize = 8 * transform.localScale.x;
        systems[0].Play();
        systems[1].Stop();
        systems[1].Clear();
        systems[2].Clear();
        systems[3].Clear();
    }

    public void OffReduce()
    {
        
        systems[1].startSize = 8 * transform.localScale.x;
        systems[1].Play();
        systems[0].Stop();
        systems[0].Clear();
        systems[2].Clear();
        systems[3].Clear();
    }

    public void OnGravity()
    {
        systems[2].Play();
        systems[3].Stop();
        systems[3].Clear();
    }

    public void OffGravity()
    {
        systems[3].Play();
        systems[2].Stop();
        systems[2].Clear();
    }

    public void OnMagic()
    {
        systems[4].startSize = 5 * transform.localScale.x;
        systems[4].Play();

    }


    void Update()
    {
        is_reduced = GetComponent<GmObj>().is_reduced;
        is_ungravited = GetComponent<GmObj>().is_ungravited;

        
    }
}